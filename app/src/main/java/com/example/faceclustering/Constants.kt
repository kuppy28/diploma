package com.example.faceclustering

const val MOBILENET_FILE_PATH = "file:///android_asset/vgg2_mobilenet.pb"
//const val MOBILENET_FILE_PATH = "file:///android_asset/age_gender_tf2_224_deep-03-0.13-0.97.pb"
const val MTCNN_FILE_PATH = "file:///android_asset/mtcnn_freezed_model.pb"
