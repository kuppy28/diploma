package com.example.faceclustering

import android.Manifest
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import kotlinx.android.synthetic.main.activity_infer_location.*
import android.widget.CompoundButton
import android.widget.Toast
import java.io.File
import java.lang.Exception
import org.json.JSONObject
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.util.Base64
import org.json.JSONArray
import java.io.ByteArrayOutputStream
import android.Manifest.permission
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.util.Log
import android.view.View
import kotlinx.coroutines.GlobalScope
import pub.devrel.easypermissions.EasyPermissions
import kotlinx.coroutines.launch
import org.christopherfrantz.dbscan.DBSCANClusterer
import org.christopherfrantz.dbscan.DBSCANClusteringException
import org.tensorflow.contrib.android.TensorFlowInferenceInterface
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class InferLocationActivity : AppCompatActivity() {
    private val galleryPermissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET)
    private lateinit var pathsToPhotos: ArrayList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_infer_location)
        pathsToPhotos = getPhotoContent()
        var pathsToClusteredPhoto = ArrayList<ArrayList<String>>()

        serverButton.setOnClickListener {
            androidButton.isClickable = false
            serverButton.isClickable = false
            progressBar.visibility = View.VISIBLE
            GlobalScope.launch {
                try {
                    val JSONForServer = createJSONForServer(pathsToPhotos)
                    val result = RemoteServer.getClusters(JSONForServer)
                    val barActivity = Intent(applicationContext, MainActivity::class.java)
                    barActivity.putExtra("clusters", result.toString())
                    startActivity(barActivity)
                } catch (e: Exception) {
                    Log.d("SERVER", e.toString())
                    e.printStackTrace()
                }
                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE
                    androidButton.isClickable = true
                    serverButton.isClickable = true
                }
            }
        }

        androidButton.setOnClickListener {
            androidButton.isClickable = false
            serverButton.isClickable = false
            progressBar.visibility = View.VISIBLE
            GlobalScope.launch {
                pathsToClusteredPhoto = startClustering(pathsToPhotos)
                val json = createJSON(pathsToClusteredPhoto)
                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE
                    androidButton.isClickable = true
                    serverButton.isClickable = true
                }
                val barActivity = Intent(applicationContext, MainActivity::class.java)
                barActivity.putExtra("clusters", json.toString())
                startActivity(barActivity)
            }
        }
    }

    fun startClustering(pathsToPhotos : ArrayList<String>): ArrayList<ArrayList<String>>  {
        val face_features = find_faces(pathsToPhotos)
        return makeCluaster(face_features)
    }

    private fun find_faces(pathsToPhotos: ArrayList<String>):  MutableMap<FloatArray, String> {
        val mtcnnModel = BaseModel.create(assets, MTCNN_FILE_PATH) as MTCNNModel
        var photoFeatures = HashMap<FloatArray, String>()
        for (path in pathsToPhotos) {
            if (!path.endsWith(".jpg")) {
                continue
            }
            val bitmapPhoto = BitmapFactory.decodeFile(path)
            val boxes = mtcnnModel.detectFaces(bitmapPhoto, 40)
            val face_features = extractFeatures(boxes, bitmapPhoto)
            photoFeatures.put(face_features, path)
        }
        return photoFeatures
    }

    private fun extractFeatures(boxes : Vector<Box>, bitmapPhoto: Bitmap) : FloatArray {
        val mobilenetModel = BaseModel.create(assets, MOBILENET_FILE_PATH) as MobilenetModel
        val face_features = mobilenetModel.extract_features(boxes, bitmapPhoto)
        return face_features
    }

    /*
       Clustering photo method
       @params: key - path to photo, value - face features
       Returns list of clusters with paths to photos
    */
    fun makeCluaster(face_features : MutableMap<FloatArray, String>): ArrayList<ArrayList<String>> {
        var pathsToClusteredPhoto = ArrayList<ArrayList<String>>()
        var clusteredPhotoFeatures = ArrayList<ArrayList<FloatArray>>()
        var featuresList = ArrayList<FloatArray>()
        for (photoFeature in face_features) {
            featuresList.add(photoFeature.key)
        }
        val ab = DistanceMetricFloatArray().calculateDistance(featuresList[0], featuresList[1])
        val ac = DistanceMetricFloatArray().calculateDistance(featuresList[0], featuresList[2])
        val bc = DistanceMetricFloatArray().calculateDistance(featuresList[1], featuresList[2])

        val ad = DistanceMetricFloatArray().calculateDistance(featuresList[0], featuresList[3])
        val dc = DistanceMetricFloatArray().calculateDistance(featuresList[3], featuresList[2])
        val bd = DistanceMetricFloatArray().calculateDistance(featuresList[1], featuresList[3])

        lateinit var clusterer :DBSCANClusterer<FloatArray>
        try {
            clusterer = DBSCANClusterer<FloatArray>(featuresList, 2, 0.04, DistanceMetricFloatArray())
        } catch (e1 : DBSCANClusteringException) {
            Toast.makeText(this, "Should not have failed on instantiation: " + e1, Toast.LENGTH_LONG).show()
            Log.d("CLUSTER", "Should not have failed on instantiation: " + e1)
        }

        try {
            clusteredPhotoFeatures = clusterer.performClustering()
        } catch (e: DBSCANClusteringException) {
            Toast.makeText(this, "Should not have failed while performing clustering: $e", Toast.LENGTH_LONG).show()
            Log.d("CLUSTER", "Should not have failed while performing clustering " + e)
        }
        // get paths from features
        // and create path-to-cluster
        for (cluster in clusteredPhotoFeatures) {
            val pathsInCluster: ArrayList<String> = ArrayList<String>()
            for (feature in cluster) {
                pathsInCluster.add(face_features[feature]!!)
            }
            pathsToClusteredPhoto.add(pathsInCluster)
        }
        return pathsToClusteredPhoto
    }

    // returns paths to photos in Gallery
    fun getPhotoContent(): ArrayList<String> {
        val pathsToPhotos = ArrayList<String>()
        try {
            val pathToDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + File.separator + "Diplom").toString()
            val directory = File(pathToDir)
            if (!directory.exists()) {
                throw NoSuchFileException(directory)
            }
            if (EasyPermissions.hasPermissions(this, *galleryPermissions)) {
                val listOfFile = directory.listFiles()
                if (listOfFile.size == 0) {
                    Toast.makeText(this, "No photos in directory diplom", Toast.LENGTH_LONG).show()
                    throw NoSuchFileException(listOfFile[0])
                }
                for (file in listOfFile) {
                    pathsToPhotos.add(file.absolutePath)
                }
            } else {
                EasyPermissions.requestPermissions(this, "Access for storage",
                    101, *galleryPermissions);
            }
        } catch (e: Exception) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
            Log.d("PHOTOS", e.toString())
        }
        return pathsToPhotos
    }

    fun createJSONForServer(pathsToPhotos :  ArrayList<String>) : JSONObject {
        val jsonObject = JSONObject()

        for (path in pathsToPhotos) {
            // todo: best resize
            var bitmap = BitmapFactory.decodeFile(path)
            val w = bitmap.width
            val h = bitmap.height
            val ratio :  Double = h.toDouble() / w.toDouble()
            val h1 = 224 * ratio
            bitmap = Bitmap.createScaledBitmap(bitmap, 224,h1.toInt(), false)
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            val byteArray = stream.toByteArray()
            val encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT)
            stream.close()
            jsonObject.put(path, encodedImage)
        }
        return jsonObject
    }

    fun createJSON(pathsToClusteredPhoto: ArrayList<ArrayList<String>>) : JSONObject {
        var jsonObject = JSONObject()

        var cnt = 0
        for (cluster in pathsToClusteredPhoto) {
            var array = JSONArray()
            for (path in cluster) {
                array.put(path)
            }
            jsonObject.put(cnt.toString(), array)
            cnt += 1
        }
        return jsonObject
    }
}
