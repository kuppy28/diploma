package com.example.faceclustering

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import android.content.Intent
import android.graphics.Color
import android.widget.Toast
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import org.json.JSONArray
import org.json.JSONObject
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis


class MainActivity : AppCompatActivity() {

    lateinit var clustered_photos: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var pathsToClusteredPhoto = ArrayList<ArrayList<String>>()
        var notClustered = ArrayList<String>()

        val clustered_photos = intent.getStringExtra("clusters")

        val result = JSONObject(clustered_photos)
        val keys = result.keys()
        while (keys.hasNext()) {
            val key = keys.next()
            // todo: if server failed
            if (key == "message") {
                Toast.makeText(this, "Server failed", Toast.LENGTH_LONG).show()
                finish()
                return
            }
            val paths_in_cluster : JSONArray = result.get(key) as JSONArray
            var clustered = ArrayList<String>()
            for (i in 0 until paths_in_cluster.length()) {
                val path = paths_in_cluster.getString(i)
                if (key != "-1") {
                    clustered.add(path)
                } else {
                    notClustered.add(path)
                }
            }
            if (clustered.size != 0) {
                pathsToClusteredPhoto.add(clustered)
            }
        }
        if (notClustered.size != 0) {
            pathsToClusteredPhoto.add(notClustered)
        }
        if (pathsToClusteredPhoto.size == 0) {
            Toast.makeText(this, "no pathes", Toast.LENGTH_LONG).show()
        }
        drawBarChart(pathsToClusteredPhoto)
    }

    // correlation between person and his/her num of photos
    fun drawBarChart(pathsToClusteredPhoto: ArrayList<ArrayList<String>>) {
        val listOfClusteredPhotoNum = ArrayList<Int>()

        pathsToClusteredPhoto.forEach { currentList -> listOfClusteredPhotoNum.add(currentList.size) }

        val barEntry = ArrayList<BarEntry>()
        val labels = ArrayList<String>();

        var maxInCluster = 0f

        for ((index, value) in listOfClusteredPhotoNum.withIndex()) {
            maxInCluster = Math.max(maxInCluster, value.toFloat())
            barEntry.add(BarEntry(index.toFloat(), value.toFloat()))
            labels.add("Person #$index")
        }
        val dataSet = BarDataSet(barEntry, "Photos")
        val data = BarData(dataSet)
        data.setValueTextSize(13f);

        bar_chart.data = data
        bar_chart.xAxis.valueFormatter = IndexAxisValueFormatter(labels)
        bar_chart.animateXY(1000, 1000);

        // Display scores inside the bars
        bar_chart.setDrawValueAboveBar(false);

        bar_chart.xAxis.labelCount = dataSet.entryCount;
        bar_chart.xAxis.position = XAxis.XAxisPosition.BOTTOM

        // Hide grid lines
        bar_chart.axisLeft.isEnabled = false;
        bar_chart.axisRight.isEnabled = false;
        // Hide graph description
        bar_chart.description.isEnabled = false;
        // Hide graph legend
        bar_chart.legend.isEnabled = false;

        bar_chart.axisLeft.axisMaximum = maxInCluster;
        bar_chart.axisLeft.axisMinimum = 0f;

        bar_chart.invalidate()

        // Choose one bar to see photos for this person
        bar_chart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                val photoIntent = Intent(applicationContext, PhotoActivity::class.java)
                // todo: possible mistake
                photoIntent.putExtra("photos", pathsToClusteredPhoto[e!!.x.toInt()])
                photoIntent.putExtra("label", "Person #" + e!!.y.toInt().toString())
                startActivity(photoIntent)
            }

            override fun onNothingSelected() {}
        })
    }
}

class XAxisValueFormatter(private val values: ArrayList<String>) : IAxisValueFormatter {

    override fun getFormattedValue(value: Float, axis: AxisBase): String {
        // "value" represents the position of the label on the axis (x or y)
        return this.values[value.toInt()]
    }

}