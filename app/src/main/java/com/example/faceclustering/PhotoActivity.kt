package com.example.faceclustering

import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity

import android.support.v4.view.ViewPager
import android.os.Bundle
import android.os.Environment
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.android.synthetic.main.activity_photo.*
import java.io.File.separator
import android.os.Environment.getExternalStorageDirectory
import java.io.File
import android.R.attr.path
import android.widget.Toast


class PhotoActivity : AppCompatActivity() {
    lateinit var userPhotosList: ArrayList<String>
    lateinit var label: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        userPhotosList = intent.getStringArrayListExtra("photos")
        label = intent.getStringExtra("label")

        viewPager.adapter = object : PagerAdapter() {
            override fun isViewFromObject(p0: View, p1: Any): Boolean {
                return p0 == p1
            }

            override fun getCount(): Int {
                return userPhotosList.size
            }

            override fun instantiateItem(container: ViewGroup, position: Int): View {
                val imageView = ImageView(this@PhotoActivity)
                imageView.setImageBitmap(BitmapFactory.decodeFile(userPhotosList[position]))

                (container as ViewPager).addView(imageView, 0)
                return imageView
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                (container as ViewPager).removeView(`object` as View)
            }
        }

        createDirectoryButton.setOnClickListener {
            // todo: create progressbar
            GlobalScope.launch {
                val success = createDirectoryAndMovePhotos()

                runOnUiThread {
                    // todo: show message that directory was created and photos were transferred
                    if (!success) {
                        Toast.makeText(this@PhotoActivity, "Directory has already existed", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    fun createDirectoryAndMovePhotos() : Boolean {
        val path = Environment.getExternalStorageDirectory().toString()
        val directory = File(path, "/appname/media/$label")
        // todo: try-check
        if (!directory.exists()) {
            directory.mkdirs()
            for (photoPath in userPhotosList) {
                val from = File(photoPath)
                val name = photoPath.subSequence(32, photoPath.length)
                val to = File(directory.toString() + File.separator + name)
                from.renameTo(to)
            }
            return true
        } else {
            if (directory.listFiles().isEmpty()) {
                for (photoPath in userPhotosList) {
                    val from = File(photoPath)
                    val name = photoPath.subSequence(32, photoPath.length)
                    val to = File(directory.toString() + File.separator + name)
                    from.renameTo(to)
                }
                return true
            }
        }
        return false
    }

}
