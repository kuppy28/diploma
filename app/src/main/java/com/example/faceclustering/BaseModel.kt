package com.example.faceclustering

import android.content.res.AssetManager
import android.graphics.Bitmap
import org.tensorflow.contrib.android.TensorFlowInferenceInterface

open class BaseModel {
    companion object {
        fun create (assetManager : AssetManager, pathToModel : String) : BaseModel {
            if (pathToModel.contains("mobilenet")) {
                return MobilenetModel(TensorFlowInferenceInterface(assetManager, pathToModel))
            }
            return MTCNNModel(TensorFlowInferenceInterface(assetManager, pathToModel))
        }
    }

    fun normalizeImage(bitmap: Bitmap): FloatArray {
        val w = bitmap.width
        val h = bitmap.height
        val floatValues = FloatArray(w * h * 3)
        val intValues = IntArray(w * h)
        bitmap.getPixels(intValues, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        val imageMean = 127.5f
        val imageStd = 128f

        for (i in intValues.indices) {
            val `val` = intValues[i]
            floatValues[i * 3 + 0] = ((`val` shr 16 and 0xFF) - imageMean) / imageStd
            floatValues[i * 3 + 1] = ((`val` shr 8 and 0xFF) - imageMean) / imageStd
            floatValues[i * 3 + 2] = ((`val` and 0xFF) - imageMean) / imageStd
        }
        return floatValues
    }
}